import { createServer } from "node:http";
import { Server } from "socket.io";
import express from "express";
import colors from "colors";
import cors from "cors";

const corsOptions = {
  origin: "http://localhost:5173",
  methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
  credentials: true,
  preflightContinue: false,
  optionsSuccessStatus: 204,
  allowedHeaders: "Content-Type,Authorization",
};

const PORT = 3000;
const app = express();
app.use(cors(corsOptions));

const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: "http://localhost:5173",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

app.get("/", (req, res) => {
  res.send("API is running");
});

io.on("connection", (socket) => {
  console.log("Id ", socket.id);

  socket.on("received-message", ({ message, room }) => {
    io.to(room).emit("received-message", message);
  });

  socket.on("disconnect", () => {
    console.log("User Disconnected", socket.id);
  });

  socket.on("connect_error", (error) => {
    console.error("Socket connection error:", error);
  });
});

server.listen(PORT, () => {
  console.log(`Server running in DEV mode on port ${PORT}`.bold.yellow);
});
