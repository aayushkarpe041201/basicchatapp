import { useEffect, useState, useMemo } from "react";
import { io } from "socket.io-client";
import { Button, Container, TextField, Typography } from "@mui/material";

function App() {
  const [socket, setSocket] = useState(null);
  const [socketId, setSocketId] = useState("");
  const [message, setMessage] = useState("");
  const [room, setRoom] = useState("");

  useEffect(() => {
    const newSocket = io("http://localhost:3000");

    newSocket.on("connect", () => {
      setSocketId(newSocket.id);
      console.log("Connected ", newSocket.id);
    });

    newSocket.on("received-message", (data) => {
      console.log(data);
    });

    newSocket.on("connect_error", (error) => {
      console.error("Socket connection error:", error);
    });

    setSocket(newSocket);

    return () => {
      newSocket.disconnect();
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (socket) {
      socket.emit("received-message", { message, room });
      setMessage("");
    } else {
      console.error("Socket connection not established.");
    }
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h1">Welcome to socket.io</Typography>

      <Typography variant="h2">{socketId}</Typography>

      <form onSubmit={handleSubmit}>
        <TextField
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          id="outlined-basic"
          label="Message"
          variant="outlined"
        />
        <TextField
          value={room}
          onChange={(e) => setRoom(e.target.value)}
          id="outlined-basic"
          label="Room"
          variant="outlined"
        />
        <Button type="submit" variant="contained" color="primary">
          Send
        </Button>
      </form>
    </Container>
  );
}

export default App;
